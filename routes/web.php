<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();


Route::get('/home',function(){
	return redirect('/dashboard');
});

Route::get('/', 'HomeController@index');


Route::get('/dashboard','StaffController@dashboard');
Route::get('/view-procedures','StaffController@viewProcedures');
Route::get('/add-procedure','StaffController@addProcedure');
Route::get('/view-refferals','StaffController@viewRefferals');
Route::get('/add-refferal','StaffController@addRefferal');
Route::get('/view-clients','StaffController@viewClients');
Route::get('/register-client','StaffController@registerClient');
Route::get('/view-faq','StaffController@viewFAQ');
Route::get('/add-faq','StaffController@addFAQ');
Route::get('/view-client/{cid}','StaffController@viewClient');

Route::get('/delete-male-procedure/{pid}','StaffController@deleteMaleProcedure');
Route::get('/delete-female-procedure/{pid}','StaffController@deleteFemaleProcedure');
Route::get('/delete-faq/{fid}','StaffController@deleteFaq');


Route::post('/register-client','StaffController@postRegisterClient');
Route::post('/add-refferal','StaffController@postAddRefferal');
Route::post('/add-procedure','StaffController@postAddProcedure');
Route::post('/add-faq','StaffController@postAddFAQ');

Route::post('/app-login','HomeController@login');
Route::post('/get-procedures','HomeController@getProcedures');
Route::post('/get-faq','HomeController@getFAQ');

Route::get('/report',function(){
	$clients = \App\client::all();
	return view('report',['clients' => $clients]);
});