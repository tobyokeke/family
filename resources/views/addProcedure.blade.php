@extends('layouts.app')

@section('content')

    <div class="container">

        @if( Session::has('error') )
            <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
        @endif

        @if( Session::has('success') )
            <div class=" alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif

        <img class="main" src="{{url('/images/logo.png')}}">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add PROCEDURE
            </div>
            <div class="panel-body">
                <form class="form-group" enctype="multipart/form-data" action="{{url('/add-procedure')}}" method="post">

                    {{csrf_field()}}


                    <div style="margin-left:15px;" class="form-group">
                        <label style="width:100px;">Gender</label>
                        <select name="gender">
                            <option>MALE</option>
                            <option>FEMALE</option>
                        </select>
                    </div>


                    <div style="margin-left:15px;" class="form-group">
                        <label style="width:100px;">Term</label>
                        <select name="type">
                            <option>SHORT TERM</option>
                            <option>LONG TERM</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <label class="col-md-4 control-label">Title</label> <br>
                        <input  type="text" class="form-control" name="title" value="{{ old('title') }}" >
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Description</label> <br>
                        <textarea class="form-control" name="description">{{old('description')}}</textarea>
                    </div>

                    <div id="preview"></div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Image</label>
                        <input type="file" onchange="readURL(this)" name="image">
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Video Link</label> <br>
                        <input  type="text" class="form-control" name="video" value="{{ old('video') }}" >
                    </div>

                    <button class="btn btn-primary">Add</button>
                    <button type="reset" class="btn btn-warning">Clear</button>


                </form>
            </div>
        </div>
    </div>

    <script>

        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<img src ="' + e.target.result + '">');
                    };

                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>

@endsection