@extends('layouts.app')

@section('content')

    <div class="container">

        @if( Session::has('error') )
            <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
        @endif

        @if( Session::has('success') )
            <div class=" alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif

        <img class="main" src="{{url('/images/logo.png')}}">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add FAQ
            </div>
            <div class="panel-body">
                <form class="form-group" action="{{url('/add-faq')}}" method="post">

                    {{csrf_field()}}


                    <div class="form-group">
                        <label for="question" class="col-md-4 control-label">Question</label> <br>
                        <input id="question" type="text" class="form-control" name="question" value="{{ old('question') }}" >
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Answer</label> <br>
                        <textarea class="form-control" name="answer">{{old('answer')}}</textarea>
                    </div>

                    <button class="btn btn-primary">Add</button>
                    <button type="reset" class="btn btn-warning">Clear</button>


                </form>
            </div>
        </div>
    </div>


@endsection